package com.company;

public class Car {

    private Information information;
    private Engine engine;
    private Transmission transmission;
    private Wheel[] wheel;

    public Car(Information information, Engine engine, Transmission transmission, Wheel[] wheel) {
        this.information = information;
        this.engine = engine;
        this.transmission = transmission;
        this.wheel = wheel;
    }

    public Information getInformation() {
        return information;
    }

    public void setInformation(Information information) {
        this.information = information;
    }

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    public Transmission getTransmission() {
        return transmission;
    }

    public void setTransmission(Transmission transmission) {
        this.transmission = transmission;
    }

    public Wheel[] getWheel() {
        return wheel;
    }

    public void setWheel(Wheel[] wheel) {
        this.wheel = wheel;
    }
}
