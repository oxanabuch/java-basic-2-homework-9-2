package com.company;

public class Wheel {

    private int width;
    private int diameter;
    private int profile;
    private String brand;

    public Wheel(int width, int diameter, int profile, String brand) {
        this.width = width;
        this.diameter = diameter;
        this.profile = profile;
        this.brand = brand;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getDiameter() {
        return diameter;
    }

    public void setDiameter(int diameter) {
        this.diameter = diameter;
    }

    public int getProfile() {
        return profile;
    }

    public void setProfile(int profile) {
        this.profile = profile;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }
}
