package com.company;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Information information = new Information("VW", "Polo", "black", 1999);
        Engine engine = new Engine(8, 1.8, 184.0);
        Transmission transmission = new Transmission("manual");
        Wheel wheel = new Wheel(205, 16, 55, "Nokian");
        Wheel[] wheels = {wheel, wheel, wheel, wheel};

        Car car = new Car(information, engine, transmission, wheels);

        String carBrand = car.getInformation().getBrand();
        String wheelBrand = car.getWheel()[0].getBrand();
        System.out.println("Car brand: " + carBrand);
        System.out.println("Wheel brand: " + wheelBrand);
        System.out.println("-------");
    }
}
